import { Module, Inject, Injectable } from '@nestjs/common';

@Injectable()
export class Foo {}

@Injectable()
export class AppService {
  constructor(@Inject('AppService') private readonly app: string) { console.log('not a self-injection thus it should work as usual') }
  // constructor(@Inject(Foo) private readonly app: any) { console.log('working as usual') }

  // constructor(@Inject('X') private readonly app: any) { console.log('self-injection is not allowed') }
  // constructor(@Inject(AppService) private readonly app: any) { console.log('self-injection is not allowed') }
}

@Module({
  providers: [
    AppService,
    Foo,

    {
      provide: 'X',
      useClass: AppService,
    },
    {
      provide: 'AppService',
      useValue: 'Some String',
    },
  ],
})
export class AppModule {}
